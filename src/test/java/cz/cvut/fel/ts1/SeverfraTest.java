package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SeverfraTest {

    @Test
    public void factorial_nIs5_returns120(){

        Blumesim severfra = new Blumesim();
        int n = 5;
        int expected = 120;

        int actual = severfra.factorial(n);


        Assertions.assertEquals(expected, actual);

    }


    @Test
    public void factorial_nIs0_returns1(){

        Blumesim severfra = new Blumesim();
        int n = 0;
        int expected = 1;

        int actual = severfra.factorial(n);


        Assertions.assertEquals(expected, actual);

    }
}
