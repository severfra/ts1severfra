package cz.cvut.fel.ts1;

public class Blumesim {

    static int factorial(int n){
        if (n == 0)
            return 1;
        else
            return(n * factorial(n-1));
    }
}
